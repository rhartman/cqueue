#include <iostream>
#include <string>

using namespace std;

#ifndef CQUEUE_CLASS_DECL
#define CQUEUE_CLASS_DECL

class qnode
{
    public:
		string data;
        qnode *prev, *next;
};

class CQUEUE
{
    public:
        CQUEUE();
        ~CQUEUE();
       CQUEUE(const CQUEUE &);
	   CQUEUE(const string);
       void Enqueue(string);
       void Dequeue();
       void Print();
	   bool isEmpty(){return (front == 0);};
	   bool isOne();
   private:
       qnode *front;
};
#endif
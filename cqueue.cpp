/*
Author:  Ryan Hartman
Course:  COP3530-Data Structures
Semester:  Fall 2011
Project:  Assignment #4
Due Date:  11-8-2011 @6:30PM EST
File Name: "cqueue.cpp"

Description:
This source file contains the definition for the class CQUEUE.  All member functions, constructors and destructors are included in
this source file.  This file depends on the cqueue.h header file which declares the class and its member functions.  The purpose
of this class is to demonstrate how a doubly-linked circular queue works.  This particular implementation does not use a back
pointer in its objects.  The last node in the object is pointed to by the front node's previous pointer.
*/

//Preprocessor directives
#include <iostream>
#include <string>
#include "cqueue.h"

//using standard namespace
using namespace std;

//Description:  Default constructor
//Preconditions:  CQUEUE object does not exist.
//Postconditions:  CQUEUE object now exists.
CQUEUE::CQUEUE()
{
	front = 0;
}

//Description:  Destructor
//Preconditions:  CQUEUE object exists
//Postconditions:  CQUEUE object's dynamic memory has been de-allocated
CQUEUE::~CQUEUE()
{
	qnode *p;
	//if only one long, delete using front pointer
	if(isOne())
	{
		delete front;
		front = 0;
		return;
	}
	//if the object is not empty
	if(!isEmpty())
	{
		//pointer p is equal to front
		p = front;
		//while you haven't reached the end of the list
		while(p != 0)
		{
			//traverse p to next element
			p = p->next;
			//delete current front
			delete front;
			//assign new value to front
			front = p;
		}
		//finally, set front to 0
		front = 0;
	}

}

//Description:  Copy constructor
//Preconditions:  A CQUEUE object may or may not exist
//Postconditions:  A CQUEUE object has been created with the contents of another CQUEUE object
CQUEUE::CQUEUE(const CQUEUE &orig)
{
	//initialize front
	front = 0;

	//if the original is not empty
	if(orig.front != 0)
	{
		//pointer p points to front of original object
		qnode *p;
		p = orig.front;
		//while p has not traversed to null
		while(p != 0)
		{
			//enqueue the data of the current element into the new object and traverse p forward
			Enqueue(p->data);
			p = p->next;
		}
	}

}

//Description:  Explicit value constructor
//Preconditions:  CQUEUE object does not exist
//Postconditions:  CQUEUE object exists with one element of the user's choice
CQUEUE::CQUEUE(const string orig)
{
	//if the argument is empty, create an empty object
	if(orig == "")
	{
		front = 0;
		return;
	}
	//otherwise...
	//make a new node
	front = new qnode;
	//point it's prev pointer to itself
	front->prev = front;
	//nothing comes next
	front->next = 0;
	//copy the data
	front->data = orig;
}

//Description:  Function Enqueue() inserts a string at the end of the queue
//Preconditions:  A CQUEUE object exists
//Postconditions:  A CQUEUE object contains a string at the back
void CQUEUE::Enqueue(string element)
{
	//exit if the element is empty
	if(element == "")
	{
		return;
	}
	//if it is empty, work with the front pointer directly
	if(front == 0)
	{
		front = new qnode;
		front->data = element;
		front->prev = front;
		front->next = 0;
	}
	//otherwise...
	else
	{
		//create pointer p and create new qnode
		qnode *p;
		p = new qnode;
		//data is entered from the formal parameters
		p->data = element;
		//this is the last element, so nothing comes next
		p->next = 0;
		//previous element is the old last element, which is pointed to by front->prev
		p->prev = front->prev;
		//p is the new last element
		front->prev = p;
		//make sure the previous element's next pointer points to this element
		p->prev->next = p;
	}
}

//Description:  Dequeue() removes an element from the front of a CQUEUE object
//Preconditions:  CQUEUE object exists
//Postconditions:  If it contains any elements, the first one has been removed
void CQUEUE::Dequeue()
{
	//if the object is not empty
	if(!isEmpty())
	{
		//pointer p points to front of object
		qnode *p;
		p = front;
		//if it is only one element long, delete the only element
		if(isOne())
		{
			delete p;
			front = 0;
			return;
		}
		//otherwise, the next element's prev pointer points to the last object
		front->next->prev = front->prev;
		//front is moved to the next object
		front = front->next;
		//p (old front) is deleted
		delete p;
	}
}

//Description:  Print() prints the contents of a CQUEUE object
//Preconditions:  A CQUEUE object exists
//Postconditions:  Its contents are displayed on the screen
void CQUEUE::Print()
{
	//if it's not empty
	if(!isEmpty())
	{
		//pointer p is assigned to front of current object
		qnode *p;
		p = front;
		//traverse until p == 0
		while(p != 0)
		{
			//display data for current node
			cout<<p->data<<endl;
			//traverse the pointer
			p = p->next;
		}
	}
}

//Description:  isOne() determines if a CQUEUE object is only one element long
//Preconditions:
//Postconditions:
bool CQUEUE::isOne()
{
	//if it's not empty
	if(!isEmpty())
	{
		//determine if the front's next pointer is null and return the result
		return front->next == 0;
	}
	//return false if it's empty
	return false;
}
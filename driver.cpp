#include <iostream>
#include <string>
#include "cqueue.h"

int main()
{
	//test default constructor
	CQUEUE test;
	//test Enqueue function
	test.Enqueue("one");
	test.Enqueue("two");
	test.Enqueue("three");
	test.Enqueue("four");
	test.Enqueue("five");
	test.Print();
	//test copy constructor
	CQUEUE test2(test);
	test2.Print();
	//test explicit-value constructor
	CQUEUE test3("Hello.");
	test3.Print();
	//Try to break Dequeue()
	test3.Dequeue();
	test3.Dequeue();
	//Try to break Print();
	test3.Print();

	return 0;
}